/* 
SHORT DESCRIPTION:
	crend - A OpenGL (3.3+) rendering library written in ansi-c99. 

ORIGINAL AUTHOR:
	Thomas Alexgaard Jensen (https://gitlab.com/Alexgaard)

   __________________________
  :##########################:
  *##########################*
           .:######:.
          .:########:.
         .:##########:.
        .:##^:####:^##:.
       .:##  :####:  ##:.
      .:##   :####:   ##:.
     .:##    :####:    ##:.
    .:##     :####:     ##:.
   .:##      :####:      ##:.
  .:#/      .:####:.      \#:. 


LICENSE:
	Copyright (c) <Thomas Alexgaard Jensen>
	This software is provided 'as-is', without any express or implied warranty.
	In no event will the authors be held liable for any damages arising from
	the use of this software.
	Permission is granted to anyone to use this software for any purpose,
	including commercial applications, and to alter it and redistribute it
	freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented; you must not
	   claim that you wrote the original software. If you use this software in
	   a product, an acknowledgment in the product documentation would be
	   appreciated but is not required.

	2. Altered source versions must be plainly marked as such, and must not be
	   misrepresented as being the original software.

	3. This notice may not be removed or altered from any source distribution.

	For more information, please refer to 
	<https://choosealicense.com/licenses/zlib/>

DESCRIPTION:

Define "CREND_IMPLEMENTATION" before include.

crend is a graphics rendering library, segmented into the following sections:

1. Error Manager
2. Shader
3. Texture
4. Mesh

For easy navigation and lookup, search for "<SECITON> DECLARATION" or 
"<SECITON> DEFINITION".

 rendering library for rendering 3D graphics using OpenGL (3.3+). 
ALTERNATIVES:

MISSING:
TODO: Integrate error manager for native error management
TODO: Add function comment blocks
TODO: Add proper soft error checking for entire library
TODO: Add GLE error call to all gl functions

CHANGELOG:
	[0.2] Relocated into seperate headers. made a proper library structure.
	[0.1] Merged all headers into one.
	      Added Namespace.
	      Started Documentation.
	      Added function comment blocks to error manager functions.
	[0.0] Initialized library.

DOCUMENTATION:

*/

#ifdef __cplusplus
extern "C" {
#endif

#ifdef CREND_IMPLEMENTATION

#ifdef CREND_ERROR_IMPLEMENTATION
#define CREND_ERROR_IMPLEMENTATION
#endif /*CREND_ERROR_IMPLEMENTATION*/

#ifdef CREND_SHADER_IMPLEMENTATION
#define CREND_SHADER_IMPLEMENTATION
#endif /*CREND_SHADER_IMPLEMENTATION*/

#ifdef CREND_TEXTURE_IMPLEMENTATION
#define CREND_TEXTURE_IMPLEMENTATION
#endif /*CREND_TEXTURE_IMPLEMENTATION*/

#ifdef CREND_MESH_IMPLEMENTATION
#define CREND_MESH_IMPLEMENTATION
#endif /*CREND_MESH_IMPLEMENTATION*/

#endif /*CREND_IMPLEMENTATION*/
/******************************************************************************/
#ifndef CREND_H
#define CREND_H

#include "src/crend_error.h"
#include "src/crend_shader.h"
#include "src/crend_texture.h"
#include "src/crend_mesh.h"

#endif /*CREND_H*/

#ifdef __cplusplus
}
#endif
